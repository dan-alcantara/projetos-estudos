import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TesteCurso {

	public static void main(String[] args) {

		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(new Curso("Python", 45));
		cursos.add(new Curso("JavaScript", 150));
		cursos.add(new Curso("Java 8", 113));
		cursos.add(new Curso("C", 55));

		cursos.sort(Comparator.comparing(Curso::getAlunos));

		cursos.stream().filter(c -> c.getAlunos() > 50).findFirst()
				.ifPresent(c -> System.out.println(c.getNome() + " - Alunos: " + c.getAlunos()));

		System.out.println("--------------");

		Stream<String> nomesAlunos = cursos.stream().map(Curso::getNome);

		nomesAlunos.forEach(System.out::println);

		System.out.println("--------------");

		cursos.stream().filter(c -> c.getAlunos() > 50).map(c -> c.getAlunos()).forEach(System.out::println);

		System.out.println("--------------");

		OptionalDouble media = cursos.stream().mapToDouble(Curso::getAlunos).average();

		media.ifPresent(System.out::println);

		System.out.println("--------------");

		cursos = cursos.stream().filter(c -> c.getAlunos() > 50).collect(Collectors.toList());

		cursos.stream().forEach(c -> System.out.println(c.getNome()));

	}

}
