import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class TesteDate {

	public static void main(String[] args) {

		LocalDate hoje = LocalDate.now();

		System.out.println(hoje.format(DateTimeFormatter.ofPattern("dd/MM/yy")));

		System.out.println("-------------------------");

		LocalDate dataFutura = LocalDate.of(2099, Month.JANUARY, 25);

		System.out.println(dataFutura);

		LocalDate dataFutura2 = LocalDate.parse("2099-01-25");

		System.out.println(dataFutura2);
		
		Period period = Period.between(hoje, dataFutura);
		
		System.out.println(period);

	}

}
