package jpa.estudos.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	static private JPAUtil instance;
	static private EntityManagerFactory emf;

	private JPAUtil() {
		emf = Persistence.createEntityManagerFactory("financas");
	}

	static public JPAUtil getInstance() {
		if (instance == null)
			instance = new JPAUtil();

		return instance;
	}

	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

}
