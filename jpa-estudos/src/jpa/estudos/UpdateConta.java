package jpa.estudos;

import javax.persistence.EntityManager;

import jpa.estudos.financas.modelo.Conta;
import jpa.estudos.util.JPAUtil;

public class UpdateConta {
	public static void main(String[] args) {

		EntityManager em = JPAUtil.getInstance().getEntityManager();

		em.getTransaction().begin();

		Conta conta = em.find(Conta.class, 1);

		conta.setTitular("Titular Teste");
		conta.setBanco("Banco Teste");

		em.getTransaction().commit();
		em.close();
		
		EntityManager em2 = JPAUtil.getInstance().getEntityManager();
		
		em2.getTransaction().begin();
		
		conta.setTitular("Carlos Alberto");
		em2.merge(conta);
		
		em2.getTransaction().commit();
		em2.close();

	}
}
