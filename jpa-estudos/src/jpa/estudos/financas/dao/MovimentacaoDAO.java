package jpa.estudos.financas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.estudos.financas.enumerator.ETipoMovimetacao;

public class MovimentacaoDAO {

	private EntityManager em;

	public MovimentacaoDAO(EntityManager em) {
		super();
		this.em = em;
	}

	public List<Object[]> getResumoMovimantacao(ETipoMovimetacao tipoMovimentacao) {
		TypedQuery<Object[]> query2 = em.createNamedQuery("ResumoMovimentacao", Object[].class);

		query2.setParameter("tipoMovimentacao", tipoMovimentacao);

		List<Object[]> results = query2.getResultList();

		return results;
	}

}
