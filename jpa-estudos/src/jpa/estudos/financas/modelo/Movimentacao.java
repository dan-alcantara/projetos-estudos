package jpa.estudos.financas.modelo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import jpa.estudos.financas.enumerator.ETipoMovimetacao;

@Entity(name = "tb_movimentacao")
@NamedQuery(name = "ResumoMovimentacao", query = "select sum(m.valor), avg(m.valor), max(m.valor), count(m) from tb_movimentacao m "
		+ "where m.tipo = :tipoMovimentacao group by m.data")
public class Movimentacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	private ETipoMovimetacao tipo;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	private String descricao;

	private BigDecimal valor;
	@ManyToOne
	private Conta conta;

	@ManyToMany
	@JoinTable(name = "tb_movimentacao_categoria", joinColumns = {
			@JoinColumn(name = "movimentacao_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "categoria_id", referencedColumnName = "id") }, uniqueConstraints = {
							@UniqueConstraint(columnNames = { "movimentacao_id", "categoria_id" }) })
	private List<Categoria> categorias;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ETipoMovimetacao getTipo() {
		return tipo;
	}

	public void setTipo(ETipoMovimetacao tipo) {
		this.tipo = tipo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

}
