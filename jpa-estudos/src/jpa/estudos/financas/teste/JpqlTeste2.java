package jpa.estudos.financas.teste;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import jpa.estudos.financas.dao.MovimentacaoDAO;
import jpa.estudos.financas.enumerator.ETipoMovimetacao;
import jpa.estudos.financas.modelo.Conta;
import jpa.estudos.util.JPAUtil;

public class JpqlTeste2 {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		EntityManager em = JPAUtil.getInstance().getEntityManager();

		Conta conta = new Conta();
		conta.setId(3);

		String jpql = "select distinct c from tb_conta c left join fetch c.movimentacoes";
		Query query = em.createQuery(jpql);
		// query.setParameter("pConta", conta);

		List<Conta> contas = query.getResultList();

		contas.forEach(c -> {
			System.out.println("Titular: " + c.getTitular());
			System.out.println("Banco: " + c.getBanco());

			if (c.getMovimentacoes() != null && !c.getMovimentacoes().isEmpty()) {
				c.getMovimentacoes().forEach(m -> System.out.println("Movimentação: " + m.getDescricao()));
			}
		});

		MovimentacaoDAO movimentacaoDAO = new MovimentacaoDAO(em);

		List<Object[]> results = movimentacaoDAO.getResumoMovimantacao(ETipoMovimetacao.SAIDA);

		results.forEach(result -> {
			BigDecimal sum = (BigDecimal) result[0];
			Double media = (Double) result[1];
			BigDecimal max = (BigDecimal) result[2];
			Long quantidade = (Long) result[3];

			System.out.println("Total gastos com movimentações: " + sum);
			System.out.println("Média dos gastos com movimetações: " + media);
			System.out.println("Maior valor gasto com movimetações: " + max);
			System.out.println("Total de movimetações: " + quantidade);
		});

		em.close();
	}
}
