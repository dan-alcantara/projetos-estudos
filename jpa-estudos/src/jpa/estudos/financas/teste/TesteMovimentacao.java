package jpa.estudos.financas.teste;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.EntityManager;

import jpa.estudos.financas.enumerator.ETipoMovimetacao;
import jpa.estudos.financas.modelo.Categoria;
import jpa.estudos.financas.modelo.Conta;
import jpa.estudos.financas.modelo.Movimentacao;
import jpa.estudos.util.JPAUtil;

public class TesteMovimentacao {
	public static void main(String[] args) {
		Conta conta = new Conta();
		conta.setAgencia("8555");
		conta.setBanco("Banco do Brasil");
		conta.setNumero("96542455");
		conta.setTitular("Titular Teste Final 2");

		Categoria categoria = new Categoria("Categoria Teste Final 2");

		Movimentacao movimentacao = new Movimentacao();
		movimentacao.setConta(conta);
		movimentacao.setData(new Date());
		movimentacao.setDescricao("Saque em caixa eletrônico");
		movimentacao.setTipo(ETipoMovimetacao.SAIDA);
		movimentacao.setValor(new BigDecimal("6587.75"));
		movimentacao.setCategorias(Arrays.asList(categoria));

		EntityManager em = JPAUtil.getInstance().getEntityManager();

		em.getTransaction().begin();
		em.persist(conta);
		em.persist(categoria);
		em.persist(movimentacao);
		em.getTransaction().commit();

		em.close();
	}
}
