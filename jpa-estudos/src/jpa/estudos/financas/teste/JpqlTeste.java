package jpa.estudos.financas.teste;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import jpa.estudos.financas.enumerator.ETipoMovimetacao;
import jpa.estudos.financas.modelo.Categoria;
import jpa.estudos.financas.modelo.Conta;
import jpa.estudos.financas.modelo.Movimentacao;
import jpa.estudos.util.JPAUtil;

public class JpqlTeste {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		EntityManager em = JPAUtil.getInstance().getEntityManager();

		Conta conta = new Conta();
		conta.setId(3);

		Categoria categoria = new Categoria();
		categoria.setId(1L);

		String jpql = "select m from tb_movimentacao m join fetch m.categorias c where m.conta = :pConta "
				+ "and m.tipo = :pTipoMovimentacao and c = :pCategoria order by m.valor desc";
		Query query = em.createQuery(jpql);
		query.setParameter("pConta", conta);
		query.setParameter("pTipoMovimentacao", ETipoMovimetacao.SAIDA);
		query.setParameter("pCategoria", categoria);

		List<Movimentacao> movimentacao = query.getResultList();

		movimentacao.forEach(m -> {
			System.out.println("Descriçao: " + m.getDescricao());
			System.out.println("Valor: " + m.getValor());
		});

		System.out.println(movimentacao.get(0).getConta().getMovimentacoes().size());

		em.close();
	}
}
