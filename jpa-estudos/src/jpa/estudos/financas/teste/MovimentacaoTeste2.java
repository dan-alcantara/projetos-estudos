package jpa.estudos.financas.teste;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.EntityManager;

import jpa.estudos.financas.enumerator.ETipoMovimetacao;
import jpa.estudos.financas.modelo.Categoria;
import jpa.estudos.financas.modelo.Cliente;
import jpa.estudos.financas.modelo.Conta;
import jpa.estudos.financas.modelo.Movimentacao;
import jpa.estudos.util.JPAUtil;

public class MovimentacaoTeste2 {

	public static void main(String[] args) {

		Cliente cliente = new Cliente();
		cliente.setNome("Marcos Antônio");
		cliente.setEndereco("Rua das Acassias");
		cliente.setProfissao("Analista de Sistemas");

		Categoria categoria1 = new Categoria("Viagem Passeio");
		Categoria categoria2 = new Categoria("Viagem Trabalho");

		EntityManager em = JPAUtil.getInstance().getEntityManager();

		Conta conta = em.find(Conta.class, 3);
		conta.setCliente(cliente);

		Movimentacao movimentacao1 = new Movimentacao();
		movimentacao1.setData(new Date());
		movimentacao1.setDescricao("Viagem para Minas Gerais");
		movimentacao1.setTipo(ETipoMovimetacao.SAIDA);
		movimentacao1.setValor(new BigDecimal("390.00"));
		movimentacao1.setCategorias(Arrays.asList(categoria1, categoria2));
		movimentacao1.setConta(conta);

		Movimentacao movimentacao2 = new Movimentacao();
		movimentacao2.setData(new Date());
		movimentacao2.setDescricao("Viagem para Rio de Janeiro");
		movimentacao2.setTipo(ETipoMovimetacao.SAIDA);
		movimentacao2.setValor(new BigDecimal("490.00"));
		movimentacao2.setCategorias(Arrays.asList(categoria1, categoria2));
		movimentacao2.setConta(conta);

		em.getTransaction().begin();

		em.persist(cliente);
		em.persist(categoria1);
		em.persist(categoria2);
		em.persist(movimentacao1);
		em.persist(movimentacao2);

		em.getTransaction().commit();

		em.close();

	}

}
