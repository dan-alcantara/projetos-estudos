package jpa.estudos;

import javax.persistence.EntityManager;

import jpa.estudos.financas.modelo.Conta;
import jpa.estudos.util.JPAUtil;

public class TestConta {

	public static void main(String[] args) {

		Conta conta = new Conta();
		conta.setTitular("Daniel Alcântara");
		conta.setBanco("Bradesco");
		conta.setAgencia("5445");
		conta.setNumero("4566588");

		EntityManager em = JPAUtil.getInstance().getEntityManager();

		em.getTransaction().begin();
		em.persist(conta);
		em.getTransaction().commit();

		em.close();

	}

}
