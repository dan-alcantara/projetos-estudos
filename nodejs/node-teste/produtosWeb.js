var http = require('http');
var ip = "localhost";
var port = 3000;

var server = http.createServer((req, res) => {
    console.log("Recebendo requisição");
    res.writeHead(200, { 'Content-Type' : 'text/html' });
    res.end("<html><body><h1>Listando os produtos</h1></body></html>");
});

server.listen(port, ip);

console.log("Servidor rodando em http://" + ip + ":" + port + "/");