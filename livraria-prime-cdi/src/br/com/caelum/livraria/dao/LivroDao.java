package br.com.caelum.livraria.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.caelum.livraria.annotation.Log;
import br.com.caelum.livraria.modelo.Livro;
import br.com.caelum.livraria.tx.Transacional;

public class LivroDao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	EntityManager em;

	private DAO<Livro> dao;

	@PostConstruct
	void init() {
		this.dao = new DAO<Livro>(this.em, Livro.class);
	}

	public Livro buscaPorId(Integer livroId) {
		return this.dao.buscaPorId(livroId);
	}

	@Transacional
	public void adiciona(Livro livro) {
		this.dao.adiciona(livro);
	}

	@Transacional
	public void atualiza(Livro livro) {
		this.dao.atualiza(livro);
	}

	@Transacional
	public void remove(Livro livro) {
		this.dao.remove(livro);
	}

	@Log
	public List<Livro> listaTodos() {
		return this.dao.listaTodos();
	}

}
