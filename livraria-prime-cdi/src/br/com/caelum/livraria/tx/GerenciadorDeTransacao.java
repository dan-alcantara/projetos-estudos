package br.com.caelum.livraria.tx;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;

@Interceptor
@Transacional
public class GerenciadorDeTransacao implements Serializable {

	private static final long serialVersionUID = -1198315063937023825L;

	@Inject
	EntityManager entityManager;

	@AroundInvoke
	public Object executaTX(InvocationContext context) throws Exception {

		entityManager.getTransaction().begin();

		Object object = context.proceed();

		entityManager.getTransaction().commit();

		return object;

	}

}
