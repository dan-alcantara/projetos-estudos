package br.com.caelum.livraria.interceptor;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import br.com.caelum.livraria.annotation.Log;

@Interceptor
@Log
public class TempoDeExecucaoInterceptor implements Serializable {

	private static final long serialVersionUID = 7850483590833454091L;

	@AroundInvoke
	public Object metricMethod(InvocationContext context) throws Exception {

		String methodName = context.getMethod().getName();
		Long timeBefore = System.currentTimeMillis();

		Object result = context.proceed();

		Long timeAfter = System.currentTimeMillis();

		System.out.println("Tempo de execução do método " + methodName + ": " + (timeAfter - timeBefore) + "ms");

		return result;

	}

}
