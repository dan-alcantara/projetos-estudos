package br.com.casadocodigo.loja.request;

import java.math.BigDecimal;

public class RequestPagamento {

	private BigDecimal value;

	public RequestPagamento() {
		super();
	}

	public RequestPagamento(BigDecimal valor) {
		super();
		this.value = valor;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
