package br.com.casadocodigo.loja.controllers;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casadocodigo.loja.models.CarrinhoCompras;
import br.com.casadocodigo.loja.request.RequestPagamento;

@RequestMapping("/pagamento")
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Controller
public class PagamentoController {

	@Autowired
	CarrinhoCompras carrinho;

	@Autowired
	RestTemplate restTemplate;

	private final String url = "http://book-payment.herokuapp.com/payment";

	@RequestMapping(value = "/finalizar", method = RequestMethod.POST)
	public Callable<ModelAndView> finalizar(RedirectAttributes attributes) {
		
		return () -> {
			ModelAndView modelAndView = new ModelAndView("redirect:/produtos");
			
			try {
				System.out.println(carrinho.getTotalCompra());
				
				String response = restTemplate.postForObject(url, new RequestPagamento(carrinho.getTotalCompra()),
						String.class);
				attributes.addFlashAttribute("sucesso", response);
				
				return modelAndView;
			} catch (HttpClientErrorException e) {
				attributes.addFlashAttribute("sucesso", "Falha no pagamento!");
				return modelAndView;
			}
		};
		
	}

}
