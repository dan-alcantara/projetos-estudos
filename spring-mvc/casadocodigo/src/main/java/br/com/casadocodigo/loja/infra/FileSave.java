package br.com.casadocodigo.loja.infra;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileSave {

	@Autowired
	private HttpServletRequest request;

	final String baseDir = "files";

	public String save(MultipartFile multipartFile) {
		try {
			String filePath = baseDir + "/" + multipartFile.getOriginalFilename();
			String absoluteFilePath = request.getServletContext().getRealPath("/") + filePath;

			multipartFile.transferTo(new File(absoluteFilePath));

			return filePath;
		} catch (IllegalStateException | IOException e) {
			throw new RuntimeException(e);
		}
	}

}
