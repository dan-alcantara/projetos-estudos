package br.com.casadocodigo.loja.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import br.com.casadocodigo.loja.enums.TipoPreco;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class CarrinhoCompras implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<CarrinhoItem, Integer> itens = new LinkedHashMap<>();

	public Collection<CarrinhoItem> getItens() {
		return itens.keySet();
	}

	public void add(CarrinhoItem item) {
		itens.put(item, getQuantidade(item) + 1);
	}

	public Integer getQuantidade(CarrinhoItem item) {
		if (!itens.containsKey(item))
			itens.put(item, 0);

		return itens.get(item);
	}

	public Integer getQuantidade() {
		return itens.values().stream().reduce(0, (next, accumulator) -> next + accumulator);
	}

	public BigDecimal getTotalItem(CarrinhoItem item) {
		if (itens.get(item) != null) {
			return item.getPreco().multiply(new BigDecimal(itens.get(item)));
		}

		return BigDecimal.ZERO;
	}

	public BigDecimal getTotalCompra() {
		BigDecimal totalCompra = BigDecimal.ZERO;

		if (!itens.isEmpty()) {
			for (CarrinhoItem item : itens.keySet()) {
				totalCompra = totalCompra.add(getTotalItem(item));
			}
		}

		return totalCompra;
	}

	public void remover(Integer produtoId, TipoPreco tipoPreco) {
		Produto produto = new Produto();
		produto.setId(produtoId);
		itens.remove(new CarrinhoItem(produto, tipoPreco));
	}

}
