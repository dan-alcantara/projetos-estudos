FROM mysql:latest
MAINTAINER Daniel Alcântara
COPY ./script.sh /script.sh
COPY ./script.sql /script.sql
RUN chmod 755 script.sh
CMD [ "./script.sh" ]
EXPOSE 3306