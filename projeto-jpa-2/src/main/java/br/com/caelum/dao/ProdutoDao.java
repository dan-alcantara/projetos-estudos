package br.com.caelum.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import br.com.caelum.model.Loja;
import br.com.caelum.model.Produto;

@Repository
public class ProdutoDao implements IProdutoDao {

	@PersistenceContext
	private EntityManager em;

	public List<Produto> getProdutos() {
		return em.createQuery("select distinct p from Produto p", Produto.class)
				.setHint("javax.persistence.loadgraph", em.getEntityGraph("produtoComCategoriaAndLoja"))
				.getResultList();
	}

	public Produto getProduto(Integer id) {
		Produto produto = em.find(Produto.class, id);
		return produto;
	}

	public List<Produto> getProdutos(String nome, Integer categoriaId, Integer lojaId) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Produto> criteriaQuery = criteriaBuilder.createQuery(Produto.class);
		Root<Produto> root = criteriaQuery.from(Produto.class);
		Predicate conjunction = criteriaBuilder.conjunction();

		root.fetch("categorias");

		if (nome != null && !nome.isEmpty()) {
			Path<String> nomePath = root.<String>get("nome");
			conjunction = criteriaBuilder.and(conjunction, criteriaBuilder.like(nomePath, "%" + nome + "%"));
		}

		if (categoriaId != null) {
			Path<Integer> categoriaPath = root.join("categorias").<Integer>get("id");
			conjunction = criteriaBuilder.and(conjunction, criteriaBuilder.equal(categoriaPath, categoriaId));
		}

		if (lojaId != null) {
			Path<Integer> lojaPath = root.<Loja>get("loja").<Integer>get("id");
			conjunction = criteriaBuilder.and(conjunction, criteriaBuilder.equal(lojaPath, lojaId));

			// Exemplo de subquery com in
			/*
			 * Subquery<Integer> subQueryLoja = criteriaQuery.subquery(Integer.class);
			 * Root<Loja> rootLoja = subQueryLoja.from(Loja.class);
			 * subQueryLoja.select(rootLoja.<Integer>get("id"));
			 * subQueryLoja.where(criteriaBuilder.equal(rootLoja.<Integer>get("id"),
			 * lojaId)); conjunction = criteriaBuilder.and(conjunction,
			 * criteriaBuilder.in(lojaPath).value(subQueryLoja));
			 */
		}

		criteriaQuery.where(conjunction);
		criteriaQuery.distinct(true);

		TypedQuery<Produto> query = em.createQuery(criteriaQuery);
		query.setHint("org.hibernate.cacheable", "true");

		List<Produto> produtos = query.getResultList();

		return produtos;

	}

	public List<Produto> getProdutosCasaDoCodigo(String nome) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Produto> criteriaQuery = criteriaBuilder.createQuery(Produto.class);
		Root<Produto> root = criteriaQuery.from(Produto.class);
		Predicate conjunction = criteriaBuilder.conjunction();

		Path<Integer> lojaPath = root.<Loja>get("loja").<Integer>get("id");
		Predicate lojaPredicate = criteriaBuilder.equal(lojaPath, 1);
		conjunction = criteriaBuilder.and(lojaPredicate);

		if (nome != null && !nome.isEmpty()) {
			Path<String> nomePath = root.<String>get("nome");
			conjunction = criteriaBuilder.and(conjunction, criteriaBuilder.like(nomePath, "%" + nome + "%"));
		}

		Predicate pAnd = criteriaBuilder.and(conjunction);

		criteriaQuery.where(pAnd);

		TypedQuery<Produto> query = em.createQuery(criteriaQuery);

		List<Produto> produtos = query.getResultList();

		return produtos;

	}

	public Double getProdutoMaisCaro() {

		Double valorMax = 0.0;

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Double> criteriaQuery = criteriaBuilder.createQuery(Double.class);
		Root<Produto> payment = criteriaQuery.from(Produto.class);

		criteriaQuery.select(criteriaBuilder.coalesce(criteriaBuilder.max(payment.<Double>get("preco")), 0.0));
		criteriaQuery.distinct(true);
		TypedQuery<Double> typedQuery = em.createQuery(criteriaQuery);
		valorMax = typedQuery.getSingleResult();

		return valorMax;

	}

	public void insere(Produto produto) {
		if (produto.getId() == null)
			em.persist(produto);
		else
			em.merge(produto);
	}

}
