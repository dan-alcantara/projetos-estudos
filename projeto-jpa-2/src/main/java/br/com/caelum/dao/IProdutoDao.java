package br.com.caelum.dao;

import java.util.List;

import br.com.caelum.model.Produto;

public interface IProdutoDao {

	public List<Produto> getProdutos(String nome, Integer categoriaId, Integer lojaId);

	public List<Produto> getProdutosCasaDoCodigo(String nome);

	public Double getProdutoMaisCaro();

}
