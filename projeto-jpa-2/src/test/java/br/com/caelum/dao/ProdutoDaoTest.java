package br.com.caelum.dao;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.caelum.config.test.AppConfTest;
import br.com.caelum.model.Produto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfTest.class })
public class ProdutoDaoTest {

	@Autowired
	private IProdutoDao produtoDao;

	@Test
	public void testGetProdutos() {
		List<Produto> produtos = produtoDao.getProdutos("Violão", 1, 2);
		System.out.println("Fim de busca produtos: -----------------------------");

		assertEquals(produtos.size(), 1);
	}

	@Test
	public void getProdutosCasaDoCodigo() {
		List<Produto> produtos = produtoDao.getProdutosCasaDoCodigo("");
		System.out.println("Fim de busca produtos casa do código: -----------------------------");

		assertEquals(produtos.size(), 2);
	}

	@Test
	public void getProdutosCasaDoCodigoByName() {
		List<Produto> produtos = produtoDao.getProdutosCasaDoCodigo("Vire o jogo");
		System.out.println("Fim de busca produtos casa do código por nome: -----------------------------");

		assertEquals(produtos.size(), 1);
	}

	@Test
	public void getProdutoMaisCaro() {
		Double result = produtoDao.getProdutoMaisCaro();
		Double resultExpect = 500.00;
		System.out.println("Fim de busca maior valor de produto: -----------------------------");

		assertEquals(result, resultExpect);
	}

}
