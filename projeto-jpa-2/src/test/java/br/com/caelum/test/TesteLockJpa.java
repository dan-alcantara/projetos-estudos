package br.com.caelum.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import br.com.caelum.config.JpaConfigurator;
import br.com.caelum.model.Produto;

public class TesteLockJpa {

	private static AnnotationConfigApplicationContext context;

	public static void main(String[] args) {

		context = new AnnotationConfigApplicationContext(JpaConfigurator.class);
		EntityManagerFactory emf = context.getBean(EntityManagerFactory.class);

		EntityManager em1 = emf.createEntityManager();
		EntityManager em2 = emf.createEntityManager();

		em1.getTransaction().begin();
		em2.getTransaction().begin();

		Produto produto = em1.find(Produto.class, 1);
		produto.setDescricao(produto.getDescricao() + "Teste edição");
		em1.lock(produto, LockModeType.PESSIMISTIC_READ);

		Produto produto2 = em2.find(Produto.class, 1);
		em2.lock(produto2, LockModeType.PESSIMISTIC_READ);

		em1.getTransaction().commit();
		em2.getTransaction().commit();

	}

}
