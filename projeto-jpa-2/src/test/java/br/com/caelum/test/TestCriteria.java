package br.com.caelum.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import br.com.caelum.model.Produto;
import br.com.caelum.util.JPAUtil;

public class TestCriteria {
	public static void main(String[] args) {
		EntityManager em = JPAUtil.getInstance().getEntityManager();
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Produto> criteriaQuery = criteriaBuilder.createQuery(Produto.class);
		criteriaQuery.from(Produto.class);
		TypedQuery<Produto> query = em.createQuery(criteriaQuery);

		List<Produto> produtos = query.getResultList();

		produtos.forEach(p -> {
			System.out.println("Nome do Produto:" + p.getNome());
			System.out.println("Descrição do Produto: " + p.getDescricao());
			System.out.println("----------------------------------------------------");
		});
	}
}
