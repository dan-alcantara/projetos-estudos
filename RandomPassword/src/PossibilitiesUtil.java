import java.util.List;

public class PossibilitiesUtil {

	public static String arrayToStringSeparatedChar(String[] possiblesPassword) {
		StringBuilder stringBuilder = new StringBuilder();

		int i = 0;
		for (String possible : possiblesPassword) {
			if (i == 0) {
				stringBuilder.append(possible);
				i++;
			} else {
				stringBuilder.append(";" + possible);
			}
		}

		return stringBuilder.toString();
	}

	public String[] calculatePossibles(List<List<String>> possiblesPin) {

		int passwordSize = possiblesPin.size();
		int sizeDigits = possiblesPin.get(0).size();
		int probabilitiesSize = (int) Math.pow(sizeDigits, passwordSize);
		int quantityReapeat = probabilitiesSize;

		String[] probabilities = new String[probabilitiesSize];

		for (List<String> digits : possiblesPin) {
			int actualValue = 0;
			quantityReapeat = quantityReapeat / sizeDigits;
			for (int i = 0; i < probabilitiesSize; i++) {
				if (probabilities[i] == null) {
					probabilities[i] = "";
				}
				probabilities[i] += digits.get(actualValue);
				if ((i + 1) % quantityReapeat == 0) {
					if (actualValue == sizeDigits - 1) {
						actualValue = 0;
						continue;
					}
					actualValue++;
				}
			}
		}

		return probabilities;
	}

}
