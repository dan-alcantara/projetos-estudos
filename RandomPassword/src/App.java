import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {

	public static void main(String[] args) {
		long startTime = 0;
		long stopTime = 0;
		long elapsedTime = 0;
		// RandomPassword randomPassword = new RandomPassword();
		String[][] possibilityArrays = { { "1", "5", "F" }, { "3", "9", "A" } };

		PossibilitiesUtil possibilitiesUtil = new PossibilitiesUtil();
		List<List<String>> possibilities = new ArrayList<>();

		for (String[] array : possibilityArrays) {
			possibilities.add(Arrays.asList(array));
		}

		startTime = System.currentTimeMillis();
		String[] calculatePossibles = possibilitiesUtil.calculatePossibles(possibilities);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Fim execução PossibilitiesUtil: " + elapsedTime);

		/*
		 * startTime = System.currentTimeMillis(); Set<String> passwordSet =
		 * randomPassword.generatePasswordSet(possibilityArrays); stopTime =
		 * System.currentTimeMillis(); elapsedTime = stopTime - startTime;
		 * System.out.println("Fim execução RamdomPassord: " + elapsedTime);
		 */

		// System.out.println(passwordSet.toString());
		System.out.println(calculatePossibles.toString());
	}

}
