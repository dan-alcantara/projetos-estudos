import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class RandomPassword {

	public Set<String> generatePasswordSet(String[][] possibilityArrays) {
		int countItens = (int) Math.pow(possibilityArrays[0].length, possibilityArrays.length);
		int countIterate = 0;
		Set<String> passwordSet = new HashSet<>();

		while (countIterate < countItens) {
			String password = "";
			int countProssibility = 0;

			while (countProssibility < possibilityArrays.length) {
				password += getRandom(possibilityArrays[countProssibility++]);
			}

			if (!passwordSet.contains(password)) {
				passwordSet.add(password);
				countIterate++;
			}

		}

		return passwordSet;
	}

	private String getRandom(String[] array) {
		int rnd = new Random().nextInt(array.length);
		return array[rnd];
	}

}
