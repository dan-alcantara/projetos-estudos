#!/bin/bash

TYPE_TAG=$1
OUTPUT="$(git tag | tail -1 | sed -e 's/v//g')"

array=(${OUTPUT//./ })

case $TYPE_TAG in
    release) array[0]=$((array[0]+1)) && array[1]=0 && array[2]=0 ;;
    bugfix) array[1]=$((array[1]+1)) && array[2]=0 ;;
    hotfix) array[2]=$((array[2]+1)) ;;
    current) echo $OUTPUT && exit 0 ;;
    *) echo "Opção Inválida!" && exit 1 ;;
esac

version="v"

for i in "${array[@]}"; do
    version=$version$i.
done

version="${version::-1}"

echo $version