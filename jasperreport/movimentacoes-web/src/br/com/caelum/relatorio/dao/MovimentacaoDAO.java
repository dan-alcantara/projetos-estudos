package br.com.caelum.relatorio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.caelum.relatorio.modelo.Movimentacao;

public class MovimentacaoDAO {

	private Connection connection;

	public MovimentacaoDAO(Connection connection) {
		super();
		this.connection = connection;
	}

	public List<Movimentacao> findAll() throws SQLException {
		List<Movimentacao> movimentacaos = new ArrayList<>();

		PreparedStatement stmt = this.connection.prepareStatement("select * from movimentacoes");
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {

			// criando o objeto Movimentacao
			Movimentacao movimentacao = new Movimentacao();
			movimentacao.setId(rs.getInt("id"));
			movimentacao.setDescricao(rs.getString("descricao"));
			movimentacao.setTipoMovimentacao(rs.getString("tipoMovimentacao"));
			movimentacao.setData(rs.getDate("data"));
			movimentacao.setValor(rs.getBigDecimal("valor"));
			movimentacao.setCategoria_id(rs.getInt("categoria_id"));
			movimentacao.setConta_id(rs.getInt("conta_id"));

			// adicionando o objeto à lista
			movimentacaos.add(movimentacao);
		}

		rs.close();
		stmt.close();

		return movimentacaos;
	}

}
