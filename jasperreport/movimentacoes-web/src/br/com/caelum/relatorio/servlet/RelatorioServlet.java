package br.com.caelum.relatorio.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.relatorio.ConnectionFactory;
import br.com.caelum.relatorio.dao.MovimentacaoDAO;
import br.com.caelum.relatorio.gerador.GeradorRelatorio;
import br.com.caelum.relatorio.modelo.Movimentacao;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@WebServlet("/movimentacoes")
public class RelatorioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			String fileName = request.getServletContext().getRealPath("/WEB-INF/jasper/movimentacoes.jasper");
			Map<String, Object> params = new HashMap<>();
			Connection connection = new ConnectionFactory().getConnection();
			MovimentacaoDAO movimentacaoDAO = new MovimentacaoDAO(connection);
			String tipoMovimentacao = request.getParameter("tipo");
			List<Movimentacao> movimentacaos = movimentacaoDAO.findAll();
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(movimentacaos);
			
			params.put("TIPO", tipoMovimentacao);
			params.put("DATA_SOURCE", new JRBeanCollectionDataSource(movimentacaos));
			
			GeradorRelatorio geradorRelatorio = new GeradorRelatorio(fileName, params, dataSource);
			geradorRelatorio.geraPDFParaOutputStream(response.getOutputStream());
			
			connection.close();
		} catch (SQLException e) {
			throw new ServletException(e);
		} 
	}
}
