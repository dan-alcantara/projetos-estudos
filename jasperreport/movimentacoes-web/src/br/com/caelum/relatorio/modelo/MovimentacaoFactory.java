package br.com.caelum.relatorio.modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MovimentacaoFactory {
	
	public static List<Movimentacao> load() {
		List<Movimentacao> movimentacaos = new ArrayList<>();
 		
		for (int x = 1; x < 50; x++) {
			Movimentacao movimentacao = new Movimentacao();
			movimentacao.setData(new Date());
			movimentacao.setDescricao("Movimentação teste " + x);
			movimentacao.setId(x);
			movimentacao.setValor(new BigDecimal("350.30"));
			
			if (x % 2 != 0)
				movimentacao.setTipoMovimentacao("SAIDA");
			else 
				movimentacao.setTipoMovimentacao("ENTRADA");
			
			movimentacaos.add(movimentacao);
		}
		
		return movimentacaos;
	}

}
