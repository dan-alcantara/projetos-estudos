package br.com.caelum.relatorio.gerador;

import java.io.OutputStream;
import java.sql.Connection;
import java.util.Map;

import org.apache.jasper.JasperException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

public class GeradorRelatorio {

	private String nomeArquivo;
	private Map<String, Object> parametros;
	private Connection connection;
	private JRDataSource dataSource;

	public GeradorRelatorio(String nomeArquivo, Map<String, Object> parametros, Connection connection) {
		this.nomeArquivo = nomeArquivo;
		this.parametros = parametros;
		this.connection = connection;
	}
	
	public GeradorRelatorio(String nomeArquivo, Map<String, Object> parametros, JRDataSource dataSource) {
		this.nomeArquivo = nomeArquivo;
		this.parametros = parametros;
		this.dataSource = dataSource;
	}

	public void geraPDFParaOutputStream(OutputStream outputStream) throws JasperException {

		try {
			JasperPrint jasperPrint = null;
			
			if (connection != null)
				JasperFillManager.fillReport(this.nomeArquivo, this.parametros, this.connection);
			else if (dataSource != null)
				JasperFillManager.fillReport(this.nomeArquivo, this.parametros, this.dataSource);
			else
				throw new JasperException("Jasperreport: Connection or datasource must be other than null.");

			JRExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
			exporter.exportReport();

		} catch (JRException e) {
			throw new RuntimeException(e);
		}
	}

}
